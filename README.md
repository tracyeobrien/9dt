# 98Point6 Drop-Token: Drop Token Test Assignment #
Read through the explanation of the game, then write a test plan to verify the implementation of the game we call 9 Drop Token (9dt) and expand the given framework with a few of your own scenarios. This test plan should exercise the the service which has already been written along with the UI that is currently in development. It should demonstrate your knowledge of testing, and your ability to write maintainable, high quality test plans. The test cases you chose to implement should exercise core scenarios of the game and demonstrate your ability to write high quality test automation.

## Rules of the Game ##
Drop Token takes place on a 4x4 grid. A token is dropped along a column and said token goes to the lowest unoccupied row of the board. A player wins when they have 4 tokens next to each other either along a row, in a column, or on a diagonal. If the board is filled, and nobody has won then the game is a draw. Each player takes a turn, starting with player 1, until the game reaches either win or draw. If a player tries to put a token in a column that is already full, that results in an error state, and the player must play again until the play a valid move.


## Example Game ##
![samplegame](https://github.com/rafastealth/9dt-test2/blob/master/sample_game.png)


## Our 9dt UI: ##

We've create a small web app that plays 9dt, it can be found here. https://droptoken.herokuapp.com/game
We've also created a not-very-good 9dt playing service which this app uses. It can be found here: (https://w0ayb2ph1k.execute-api.us-west-2.amazonaws.com/production). It takes a GET param called "moves" that is a JSON array of all the moves that have taken place from the beginning of the game and returns that array plus it's move. It will 400 when it's given an invalid set of moves - for example if a column has too many tokens in it - but it can not tell if the player has won or lost.
Here is an example call for a game where player 1 went in column 0, 3 and 3 and player 2 went in column 0 and 2:https://w0ayb2ph1k.execute-api.us-west-2.amazonaws.com/production?moves=[0,0,3,2,3]

## The Provided Framework: ##

We've provided a basic, groovy/java based framework which utilizes [geb](http://www.gebish.org/manual/current/), [cucumber](https://github.com/cucumber/cucumber-jvm) and [geb-cucumber](https://github.com/tomdcc/geb-cucumber). You can utilize this provided framework or put together your own.

To build and run all the tests, run the following command:
```bash
> mvn clean test
```



## Requirements: ##
The test plan and code should include the following:
* A list of tests validating the service endpoint
* Tests validating the yet to be created UI, based on the partial mock up above, and the rules given
* Priorities for each test case
* Two or more test case added to the provided framework



## Submitting your solution ##

Please submit your source code and instructions for building/running it along with your test plan to your 98point6 contact.

To submit the source code, the preferred way is to share a Github or BitBucket repository with us. Alternatively, we can accept compressed tarballs or zip archives. We cannot accept those over email, though, so we recommend a file sharing service like Google Drive, Dropbox, or similar.

**Please submit your solution by 12pm (noon) the day before your interview.**

A starting point shim for your code is provided in the hope it will reduce boiler-plate code and some ramp-up in unknown technologies. Feel free to use/ignore it if you feel like building your own framework.

If you choose not to use the provided shim, you must provide thorough instructions on how to setup and run your solution. We are experienced developers, but we may not be familiar with the tools or languages you used, so please draft the instructions accordingly.
