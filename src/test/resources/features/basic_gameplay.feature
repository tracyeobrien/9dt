Feature: Basic Gameplay Functionality

  Background:
    #TODO: ideally eventually we would be resetting to this state instead of using UI automation to reach it
    Given I go to the landing page
    And I click the play button
    Then I am at the new game page
    And I click the start game button
    Then I am at the game in progress page
    And eventually the third play button is visible

  Scenario: first move (3,0)
    When the player clicks the 1st play button
    Then there is an "X" in position 3,0
    And the score is 0 wins, 0 losses, and 0 ties

  Scenario: resetting the game
    Given the player clicks the 1st play button
    And eventually the reset button is visible
    When I click the reset button
    Then eventually the pass button is visible

  Scenario: passing the first turn
    Given I click the pass button
    Then there is 1 "O" in the 4th row

  Scenario: making the second move
    Given the player clicks the 1st play button
    And the player clicks the 1st play button
    Then there are 2 "X" in the 1st column
    And the score is 0 wins, 0 losses, and 0 ties
