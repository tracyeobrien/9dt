Feature: Landing Page

  Scenario: Can start game
    Given I go to the landing page
    And I click the play button
    Then I am at the new game page
    And I click the start game button
    Then I am at the game in progress page