package steps

import static cucumber.api.groovy.EN.Then
import static cucumber.api.groovy.EN.When


Then(~/^there is an "([^"]*)" in position (\d+),(\d+)$/) { String value, int row, int column ->
  page.verifyPlay(row, column, value)
}

Then(~/^there (is|are) (\d) "([^"]*)" in the (\d)(st|nd|rd|th) (row|column)$/) {
  String verb, int count, String value, int position, String ordinal, String axis ->
  switch (axis) {
    case "row:":
      page.verifyRow(count, position - 1, value)
      break
    case "column":
      page.verifyColumn(count, position - 1, value)
  }
}

When(~/^the player clicks the (\d)(st|nd|rd|th) play button$/) { int column, String ordinal ->
  switch(column){
    case 1 :
      page.firstPlayButton.click()
      break
    case 2:
      page.secondPlayButton.click()
      break
    case 3:
      page.thirdPlayButton.click()
      break
    case 4:
      page.fourthPlayButton.click()
      break
  }
}

Then(~/^the score is (\d+) wins, (\d+) losses, and (\d+) ties$/){ int wins, int losses, int ties ->
  page.verifyScore(wins, losses, ties)
}