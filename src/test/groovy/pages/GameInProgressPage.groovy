package pages

import geb.Page
import org.openqa.selenium.By

class GameInProgressPage extends Page {
  static atCheckWaiting = [10, 0.5]
  static at = { header.isDisplayed() && (passButton.isDisplayed() || resetButton.isDisplayed()) }
  static content = {
    header(required: true) { $('h1') }
    passButton(required: true) { $('input', value: 'Pass') }
    playButtons(required: true) {$(By.xpath("//input[@class='btn btn-primary' and @value='Play']"))}
    resetButton(required: true) {$(By.xpath("//input[@class='btn btn-primary' and @value='Reset']"))}
    firstPlayButton(required: true) {playButtons[0]}
    secondPlayButton(required: true) {playButtons[1]}
    thirdPlayButton(required: true) {playButtons[2]}
    fourthPlayButton(required: true) {playButtons[3]}
    rows(required: true) {$(By.xpath("//div[@class='level']"))}
    winsData(required: false){$('td', class: 'record_wins')}
    lossesData(required: false){$('td', class: 'record_losses')}
    tiesData(required: false){$('td', class: 'record_ties')}
  }

  def verifyPlay(row,column, value){
    waitFor(5){
      rows[row].children()[column].text() != ""
    }
    def columnSelected = rows[row].children()[column]
    assert columnSelected.text() == value
  }

  def verifyScore(wins, losses, ties){
    assert winsData.text().toInteger() == wins
    assert lossesData.text().toInteger() == losses
    assert tiesData.text().toInteger() == ties
  }

  def verifyRow(count, row, value){
    //TODO: These waits are a little inelegant and can result in false-positives in some circumstances
    waitFor (5){
      rows[row].children().findAll{it.text() == value}
    }
    def matchingValues = rows[row].children().findAll{it.text() == value}
    assert matchingValues.size() == count
  }

  def verifyColumn(count, column, value){
    waitFor (5){
      rows.findAll{it.children()[column].text() == value}
    }
    def matchingRows = rows.findAll{it.children()[column].text() == value}
    assert matchingRows.size() == count
  }
}
